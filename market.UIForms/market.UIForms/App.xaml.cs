﻿using market.UIForms.ViewModels;
using market.UIForms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace market.UIForms
{
	public partial class App : Application
	{
        public static NavigationPage Navigator { get; internal set; }

        public static MasterPage Master { get; internal set; }

        public App ()
		{
			InitializeComponent();


            MainViewModel.GetInstance().Login = new LoginViewModel();
            MainPage = new NavigationPage( new LoginPage());


		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}


    }
}
