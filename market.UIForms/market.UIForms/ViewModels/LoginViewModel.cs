﻿

namespace market.UIForms.ViewModels
{
    using System.ComponentModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using market.Common.Models;
    using market.Common.Services;
    using market.UIForms.Views;
    using Xamarin.Forms;

    public class LoginViewModel: BaseViewModel
    {
        private ApiService apiService;
        private NetService netService;
        private bool isRunning;
        private bool isEnabled;


       // public event PropertyChangedEventHandler PropertyChanged;

        //Old Way with INotifyChanged
        //public bool IsRunning
        //{
        //    get
        //    {
        //        return this.isRunning;
        //    }

        //    set
        //    {
        //        if (this.isRunning != value)
        //        {
        //            this.isRunning = value;
        //            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
        //        }
        //    }
        //}
        //New ->
        public bool IsRunning
        {
            get => this.isRunning;
            set => this.SetValue(ref this.isRunning, value);
        }

        public bool IsEnabled
        {
            get => this.isEnabled;
            set => this.SetValue(ref this.isEnabled, value);
        }

        public string Email { get; set; }

        public string Password { get; set; }

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }

        }

        public LoginViewModel()
        {
            this.apiService = new ApiService();
            this.netService = new NetService();
            Email = "fabs@gmail.com";
            Password = "123456";
            this.IsEnabled = true;
        }


        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an password",
                    "Accept");

                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Password can't be empty",
                    "Accept");

                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            var request = new TokenRequest
            {
                Password = this.Password,
                Username = this.Email
            };

            var url = Application.Current.Resources["UrlAPI"].ToString();

            var response = await this.apiService.GetTokenAsync(
                url,
                "/Account",
                "/CreateToken",
                request);

            this.IsRunning = false;
            this.IsEnabled = true;
            var connection = await this.netService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Email or password incorrect,",
                    "Accept");

                return;
            }

            var token = (TokenResponse)response.Result;
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;


            MainViewModel.GetInstance().Products = new ProductsViewModel();
            //await Application.Current.MainPage.Navigation.PushAsync(new ProductsPage());
            Application.Current.MainPage = new MasterPage();

        }
    }
}
