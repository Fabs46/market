﻿using market.Common.Models;
using market.Common.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace market.UIForms.ViewModels
{
    public class ProductsViewModel: INotifyPropertyChanged
    {
        #region Attributes

        private ApiService apiService;
        private NetService netService;

        private ObservableCollection<Product> products;

        private bool isRefreshing;

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        //TODO: Colocar INotify genérico
        public ObservableCollection<Product> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                if(this.products != value)
                {
                    this.products = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Products"));
                }
            }
        }


        public bool IsRefreshing
        {
            get
            {
                return this.isRefreshing;
            }

            set
            {
                if (this.isRefreshing != value)
                {
                    this.isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRefreshing"));
                }
            }
        }
        

        #endregion

        #region Constructors

        public ProductsViewModel()
        {
            this.apiService = new ApiService();
            this.netService = new NetService();
            this.LoadProducts();
        }

        #endregion


        private async void LoadProducts()
        {
            this.IsRefreshing = true;


            var url = Application.Current.Resources["UrlAPI"].ToString();
            var response = await this.apiService.GetListAsync<Product>(
                url,
                "/api",
                "/Products",
                "bearer",
                MainViewModel.GetInstance().Token.Token);

            this.IsRefreshing = false;

            var connection = await this.netService.CheckConnection();
            if(!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");

                return;
            }

            var myProducts = (List<Product>)response.Result;
            this.Products = new ObservableCollection<Product>(myProducts);

        }
    }
}
