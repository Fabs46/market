﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace market.UIForms.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SetupPage : Xamarin.Forms.ContentPage
	{
		public SetupPage ()
		{
			InitializeComponent ();
		}
	}
}