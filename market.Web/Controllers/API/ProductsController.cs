﻿using market.Web.Data;
using market.Web.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace market.Web.Controllers.API
{
    [Route("api/[Controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductsController:Controller
    {
        private readonly IProductRepository productRepository;
        private IUserHelper UserHelper { get; }


        public ProductsController(IProductRepository productRepository, IUserHelper userHelper)
        {
            this.productRepository = productRepository;
            UserHelper = userHelper;
        }

        

        [HttpGet]
        public IActionResult GetProducts()
        {
             //var a = this.productRepository.GetAll().ToString();

            return Ok(this.productRepository.GetAllWithUsers());

            

        }
    }
}
