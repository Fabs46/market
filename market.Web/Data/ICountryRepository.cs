﻿

namespace market.Web.Data
{
    
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Entities;


    public interface ICountryRepository: IGenericRepository<Country>
    {
    }
}
