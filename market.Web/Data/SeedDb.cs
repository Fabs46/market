﻿
namespace market.Web.Data
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Entities;
    using market.Web.Helpers;
    using Microsoft.AspNetCore.Identity;

    public class SeedDb
    {
        public readonly DataContext context;
        private readonly IUserHelper userHelper;
        private Random random;

        public SeedDb(DataContext context,IUserHelper userHelper)
        {
            this.context = context;
            this.userHelper = userHelper;

            this.random = new Random();
        }

        public UserManager<User> UserManager { get; }
        public IUserHelper UserHelper { get; }

        public async Task SeedAsync()
        {
            await this.context.Database.EnsureCreatedAsync();

            var user = await this.userHelper.GetByEmailAsync("fabio.sarreira@gmail.com");
            if (user == null)
            {
                user = new User
                {
                    FirstName = "Fábio",
                    LastName = "Sarreira",
                    Email = "fabio.sarreira@gmail.com",
                    UserName = "fabio.sarreira@gmail.com",
                    PhoneNumber = "912331444"
                };

                var result = await this.userHelper.AddUserAsync(user,"123456");
                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Could not create the user in seeder");
                }

            }


            if(!this.context.Products.Any())
            {
                this.AddProduct("IPhone X", user);
                this.AddProduct("Rato Mickey", user);
                this.AddProduct("IWatch 1", user);

                await this.context.SaveChangesAsync();
            }
        }

        private void AddProduct(string name, User user)
        {
            this.context.Products.Add(new Product
            {
                Name = name,
                Price = this.random.Next(1000),
                IsAvailable = true,
                Stock = this.random.Next(200),
                User = user
            });
        }
    }
}
