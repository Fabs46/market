﻿

namespace market.Web.Helpers
{
    using System.Threading.Tasks;
    using market.Web.Data.Entities;
    using market.Web.Models;
    using Microsoft.AspNetCore.Identity;


    public interface IUserHelper
    {
        Task<User> GetByEmailAsync(string email);

        Task<IdentityResult> AddUserAsync(User user, string password);


        Task<SignInResult> LoginAsync(LoginViewModel model);

        Task LogoutAsync();

        Task<IdentityResult> UpdateUserAsync(User user);

        Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword);

        Task<SignInResult> ValidatePasswordAsync(User user, string password);
    }
}
