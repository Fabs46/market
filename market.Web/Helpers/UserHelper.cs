﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using market.Web.Data.Entities;
using market.Web.Models;
using Microsoft.AspNetCore.Identity;

namespace market.Web.Helpers
{
    public class UserHelper : IUserHelper
    {

        private UserManager<User> UserManager { get; }
        public SignInManager<User> SignInManager { get; }

        //
        public UserHelper(UserManager<User> UserManager, 
            SignInManager<User> signInManager)
        {
            this.UserManager = UserManager;
            SignInManager = signInManager;
        }

        

        public async Task<IdentityResult> AddUserAsync(User user, string password)
        {
            return await this.UserManager.CreateAsync(user, password);
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            return await this.UserManager.FindByEmailAsync(email);
        }

        public async Task<SignInResult> LoginAsync(LoginViewModel model)
        {
            return await this.SignInManager.PasswordSignInAsync(
                model.UserName,
                model.Password,
                model.RememberMe,
                false);
        }

        public async Task LogoutAsync()
        {
            await this.SignInManager.SignOutAsync();
        }

        public async Task<IdentityResult> UpdateUserAsync(User user)
        {
            return await this.UpdateUserAsync(user);
        }

        public async Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword)
        {
            return await this.ChangePasswordAsync(user,oldPassword, newPassword);
        }

        public async Task<SignInResult> ValidatePasswordAsync(User user, string password)
        {
            return await this.SignInManager.CheckPasswordSignInAsync(
                user,
                password,
                false
                );
        }
    }
}
