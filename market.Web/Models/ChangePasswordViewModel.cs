﻿
namespace market.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;


    public class ChangePasswordViewModel
    {
        [Required]
        [MinLength(6)]
        [Display(Name = "Current Password")]
        public string OldPassword { get; set; }


        [Required]
        [MinLength(6)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword")]
        public string Confirm { get; set; }


    }
}
